#[macro_use]
extern crate rocket;
use rocket_dyn_templates::Template;

fn context() -> impl rocket::serde::Serialize {
    // TODO: replace with context!{} when available
    // https://api.rocket.rs/master/rocket_dyn_templates/macro.context.html
    #[derive(rocket::serde::Serialize)]
    #[serde(crate = "rocket::serde")]
    struct TemplateContext {
        code_url: &'static str,
    }
    TemplateContext {
        code_url: "https://codeberg.org/sphi/FocalLink/",
    }
}

#[get("/")]
fn index() -> Template {
    Template::render("index", context())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Template::fairing())
        .mount("/", routes![index])
}
