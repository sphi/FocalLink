# FocalLink
_Design for a flexible, user-centric social media platform_

## Introduction
This is the early design for a social network. It's primary goal is to create a space where the TPoT twitter community can flourish without being encumbered by the twitter juggernaut. It also takes inspiration from Reddit, Tumblr, Mastodon, etc.

## Users
Users, or accounts, generally map 1:1 with people. People can make alt-accounts if they desire to keep posts completely separate, but many of the usecases of twitter alts are covered by streams (see below).

## Posts
Content comes in the form of posts. Posts are markdown documents that can be as short as a tweet or as long as a blog post (though long posts are often displayed in a short form with a __show more__ button). Presumably there will be support for images, videos, etc. Posts might also be stylable beyond what markdown supports. Each post is from one user, which is visible to everyone who can see the post.

## Up-links
Each post can have zero or more "up-links", which are references to existing posts. Up-links can be anchored to the entire post, or a single sentence or word. They can link to an entire existing post, or be references to a particular sentence or word within them. When a post is displayed, the posts it up-links to are displayed above.

## Down-links
When someone makes a post that up-links to an existing post, the existing post gets a down-link to the new one. In twitter terms, down-links are replies and quote-tweets. A user generally gets notified of new down-links. Down-linked posts are displayed below the current post.

## Streams
Streams are how content is organized. They are similar to twitter home/lists, but much more powerful. Each stream is owned by a user. When the owner makes a post, they can choose which of their stream(s) it goes to. They can also compose streams out of their or other people's streams. Streams can be public, private or only visible to specific users. There might also be a system for granting non-owners permission to post to a stream or edit what streams it is composed of.

## Default Streams
When a user signs up, they get several streams automatically. For example mine would look like:
- `sphi:feed`
- `sphi:main`
- `sphi:public`
- `sphi:all`

Lets break that down. `feed` is what you see by default when you open the app. `main` is where your new posts go by default. `public` is all of your posts that are visible to everyone (if you post to any public stream it must go here). `all` is all of your posts.

## Following
To "follow" an account, you add their `main` stream to your `feed` stream. This way, when they post content it will show up on your feed.

## Advanced Streams
As you can probably see, this system is very powerful. You can create additional streams (`tech`, `shitpost`, etc). You can follow or not follow any stream from any user. You can create multiple `feed` streams for different types of content. You can make private and public streams, and grant different people access to different private streams. You can probably do things I haven't thought of yet.

## Algorithm
Algorithms are great. Being psychologically manipulated by tech giants without consent, not so much. Algorithms can optionally be applied to streams, and will be fully under the user's control. For example, you could make one sub-stream show up first, or always be in chronological order while another sub-stream is sorted by amount of recent engagement.

## Likes
I want a system more flexible and quirky than a generic like button or reactions. Perhaps users, if they want this type of engagement, have to explicitly add it to their posts. This is an open question.

## Notifications
Users can be notified when they get down-links, when their streams get added to other streams (superset of being followed) and when people interact with their posts in currently undefined other ways. Notifications can be disabled or adjusted as the user wishes.

## Editing
Posts are editable. Keeping down-links organized, preventing editing abuse that changes the context of down-links, etc will be a challenge. It's worth solving though, because without editable content you can't have rich and correct information sharing.

## Customization
I think it would be cool if users, posts and/or streams could have custom styles. A bit like adding custom CSS to a Tumblr, except scrolling your feed the style is different for every post. This might be a design disaster or it might be really cute and useful or it might be both, worth looking into.

## Content Tagging
It's important that the platform be friendly and safe to anyone who can use social media. It's also important to not let that limit what content is acceptable. We need higher fidelity content tagging than just "SFW"/"NSFW". Mastodon's content warning system works ok and has lots of cool secondary uses, so that's an option. It might also be a good idea to have multiple pre-defined categories of content users may want to filter. It should probably be possible to tag content after it's posted, and have community/moderators tag content even when the OP doesn't.

## Federation
I would love to support ActivityPub (and thus be part of the fediverse, like Mastodon). I'm not willing to significantly sacrifice on UX or features for that, however. Users will have as much access to and control over their data as possible and no design decisions will be made to intentionally trap people on the platform, but federation isn't an initial priority.

## Name
The platform is called FocalLink partly because I think it's cool and relevant and mostly because I already own [focal.link](focal.link).
